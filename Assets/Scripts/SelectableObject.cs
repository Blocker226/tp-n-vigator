﻿using UnityEngine;

public class SelectableObject : MonoBehaviour {

    public virtual void Focus() {
        Debug.Log("Focus!");
    }

    public virtual void Defocus() {
        Debug.Log("Defocus!");
    }
}
