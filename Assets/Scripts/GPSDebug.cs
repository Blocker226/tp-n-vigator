﻿using UnityEngine;
using TMPro;

public class GPSDebug : MonoBehaviour {

    public TextMeshProUGUI textMesh;

	public void Calibrate() {
        BTManager.instance.offset = -BTManager.instance.marker.position;
        BTManager.instance.marker.position = Vector3.zero;
        Debug.Log("Calibrating!");
    }

    void Update() {
        textMesh.text = "Lat: " + BTManager.instance.latitude +
            "\nLong: " + BTManager.instance.longitude +
            "\nX: " + BTManager.instance.marker.position.x +
            "\nZ: " + BTManager.instance.marker.position.z +
            "\nY: " + BTManager.instance.marker.position.y;
    }
}
