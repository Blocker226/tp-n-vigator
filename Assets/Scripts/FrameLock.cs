﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameLock : MonoBehaviour {

    [SerializeField]
    int frameRate = 60;

    // Use this for initialization
    void Awake() {
        //Set target framerate
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = frameRate;
    }

    // Update is called once per frame
    void Update() {

    }
}
