﻿using UnityEngine;
using System.Collections;

public class Minimap : MonoBehaviour {

	public Transform target;
	public float zoom = 10f;
	public RectTransform boundary;

	Vector2 xRotate = Vector2.right;
	Vector2 yRotate = Vector2.up;

	void LateUpdate() {
		xRotate = new Vector2(target.right.x, -target.right.z);
		yRotate = new Vector2(-target.forward.x, target.forward.z);
	}

	public Vector2 TrackPosition (Vector3 position) {
		Vector3 offset = position - target.position;
		Vector2 newPosition = offset.x * xRotate;
		newPosition += offset.z * yRotate;
		newPosition *= zoom;
		return newPosition;
	}

	public Vector3 TrackRotation (Vector3 rotation) {
		return new Vector3(0, 0, target.eulerAngles.y - rotation.y);
	}

	public Vector2 EdgeGuide (Vector2 point) {
		point = Vector2.Max(point, boundary.rect.min);
		point = Vector2.Min(point, boundary.rect.max);
		return point;
	}
}
