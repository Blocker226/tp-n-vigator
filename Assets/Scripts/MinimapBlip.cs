﻿using UnityEngine;
using System.Collections;

public class MinimapBlip : MonoBehaviour {

	public bool edgeGuided = true;
	public bool rotationLocked = false;
	public Transform target;

	Minimap map;
	RectTransform thisRect;

	// Use this for initialization
	void Start () {
		map = GetComponentInParent<Minimap>();
		thisRect = GetComponent<RectTransform>();
	}
	
	void LateUpdate () {
		Vector2 newPosition = map.TrackPosition(target.position);
		if (edgeGuided) {
			newPosition = map.EdgeGuide(newPosition);
		}
		if (!rotationLocked) {
			thisRect.localEulerAngles = map.TrackRotation(target.eulerAngles);
		}
		thisRect.localPosition = newPosition;
	}
}
