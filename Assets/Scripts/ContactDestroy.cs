﻿using UnityEngine;

public class ContactDestroy : MonoBehaviour {
	void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") {
            Destroy(gameObject);
        }
	}
}
