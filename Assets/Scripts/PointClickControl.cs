﻿using UnityEngine;
using UnityEngine.EventSystems;
using Opsive.ThirdPersonController;


/// <summary>
/// Allows the player to click to move the character to a position. Will translate the NavMeshAgent desired velocity into values that the RigidbodyCharacterController can understand.
/// Modified by Blocker226.
/// </summary>
public class PointClickControl : NavMeshAgentBridge {
    public Transform target;
    public bool moveTowards = false;
    public LayerMask filterMask;

    // Internal variables
    private Vector3 m_Velocity;
    private Quaternion m_LookRotation;
    private bool m_AllowGameplayInput = true;
    private float distance;
    private RigidbodyCharacterController controller;

    // Component references
    private Camera m_Camera;

    /// <summary>
    /// Cache the component references and initialize the default values.
    /// </summary>
    protected override void Awake() {
        base.Awake();

        m_Camera = Utility.FindCamera(gameObject);

        SharedManager.Register(this);
    }

    /// <summary>
    /// Ensure the controller is set to the correct movement type and registered for any interested events.
    /// </summary>
    private void Start() {
        EventHandler.RegisterEvent<bool>(gameObject, "OnAllowGameplayInput", AllowGameplayInput);
        if (target == null) {
            target = GameObject.FindGameObjectWithTag("Marker").transform;
        }
        controller = GetComponent<RigidbodyCharacterController>();
#if UNITY_EDITOR || DLL_RELEASE
        // The controller must use the PointClick movement type with this component.
        if (controller.Movement != RigidbodyCharacterController.MovementType.PointClick) {
            Debug.LogWarning("Warning: The PointClickControllerHandler component has been started but the RigidbodyCharacterController is not using the PointClick movement type.");
        }
#endif

    }

    public void SelectObject(Vector2 position) {
        if (m_AllowGameplayInput && !EventSystem.current.IsPointerOverGameObject()) {
            RaycastHit hit;
            if (Physics.Raycast(m_Camera.ScreenPointToRay(position), out hit, Mathf.Infinity, filterMask)) {
                Debug.Log("Tap select on " + hit.transform.name);
                    if (hit.transform.GetComponent<SelectableObject>()) {
                        SelectableObject target = hit.transform.gameObject.GetComponent<SelectableObject>();
                        if (target != BTManager.instance.currentFocus) {
                            BTManager.instance.TryResetFocus();
                            BTManager.instance.FocusObject(target);
                        }
                    }
                    else {
                        Debug.LogWarning("Object set as selectable has no SelectableObject component!");
                    }
            }
        }
    }

    /// <summary>
    /// Move towards the mouse position if the MoveInput has been pressed. Translates the NavMeshAgent desired velocity into values that the RigidbodyCharacterController can understand.
    /// </summary>
    protected override void FixedUpdate() {
        distance = Vector3.Distance(target.position, transform.position);
        if (distance > m_NavMeshAgent.stoppingDistance) {
            m_NavMeshAgent.SetDestination(target.position);
        }
        base.FixedUpdate();
    }

    /// <summary>
    /// Is gameplay input allowed? An example of when it will not be allowed is when there is a fullscreen UI over the main camera.
    /// </summary>
    /// <param name="allow">True if gameplay is allowed.</param>
    private void AllowGameplayInput(bool allow) {
        m_AllowGameplayInput = allow;
    }
}
