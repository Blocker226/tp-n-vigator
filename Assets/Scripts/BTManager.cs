﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

public class BTManager : MonoBehaviour {

    [TabGroup("Prefabs")]
    public GameObject waypointPrefab;
    [TabGroup("Prefabs")]
    public Transform marker;
    [TabGroup("Data")]
    public SetDestination currentDestination = null;
    [TabGroup("Data")]
    public SelectableObject currentFocus = null;
    [TabGroup("Data")]
    public float latitude = 0;
    [TabGroup("Data")]
    public float longitude = 0;
    [BoxGroup("Calibration")]
    public float r = 6371;
    [BoxGroup("Calibration")]
    public Vector3 offset = Vector3.zero;
    [BoxGroup("Calibration")]
    public float scale = 0.1f;
    [TabGroup("Debug")]
    public bool moveMarker = false;

    PathTracer pathTracer;
    Transform player;
    LocationInfo prevInfo;

    public static BTManager instance = null;
    private void Awake() {
        #region Singleton/DontDestroy
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        #endregion
        currentDestination = null;
        currentFocus = null;
        pathTracer = GetComponent<PathTracer>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    /* OK FROM THE TOP
     * 1. Use iBeacons to triangulate position
     * 2. Set marker
     * 3. ???
     * 4. Profit!
     */

    public void FocusObject(SelectableObject selectable) {
        currentFocus = selectable;
        currentFocus.Focus();
    }

    public void ResetFocus() {
        currentFocus.Defocus();
        currentFocus = null;
    }

    public void TryResetFocus() {
        if (instance.currentFocus != null) {
            instance.ResetFocus();
        }
    }

    // Use this for initialization
    void Start() {
        Debug.Log(BluetoothState.GetBluetoothLEStatus());
        iBeaconReceiver.BeaconRangeChangedEvent += OnBeaconRangeChange;
        BluetoothState.BluetoothStateChangedEvent += (state) =>
        {
            if (state == BluetoothLowEnergyState.POWERED_ON)
            {
                Debug.Log("Scanning!");
                iBeaconReceiver.Scan();
            }
        };
        BluetoothState.EnableBluetooth();
    }

    void OnBeaconRangeChange(Beacon[] beacons) {
        foreach(Beacon beacon in beacons)
        {
            Debug.Log("Beacon found. Region Name: " + beacon.regionName + " Distance: " + beacon.accuracy);
        }
    }

    // Update is called once per frame
    void Update() {
        if (currentDestination != null && Vector3.Distance(player.position, currentDestination.GetPosition()) > (waypointPrefab.GetComponent<SphereCollider>().radius)) {
            pathTracer.MakePath(player.position, currentDestination.GetPosition());
        }
        else {
            pathTracer.DestroyPath();
        }
    }

    [Button]
    public void UpdateMarker() {
        marker.position = offset;
        prevInfo = Input.location.lastData;
    }
}
