﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Destination", menuName = "Map Navigation/GPS Destination")]
public class GPSDestination : ScriptableObject {

    public new string name = "Destination";
    public string[] locationTags;
    public bool isImportant = false;
    public string description = "Description";
    public bool favourite = false;
    public int favouriteOrder = 0;
}
