﻿using UnityEngine;
using HedgehogTeam.EasyTouch;
using System.Collections;

public class TouchOrbit : MonoBehaviour {

    public Transform target;
    public float distance = 1.5f;
    public int cameraSpeed = 5;
    public float xSpeed = 175.0f;
    public float ySpeed = 75.0f;
    public float pinchSpeed = 1;
    private float lastDist = 0;
    public int yMinLimit = 10; //Lowest vertical angle in respect with the target.
    public int yMaxLimit = 80;
    public float minDistance = .5f; //Min distance of the camera from the target
    public float maxDistance = 1.5f;
    private float x = 0.0f;
    private float y = 0.0f;

    void Start() {

        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

    }

    public void OrbitCamera(Vector2 deltaPosition) {
        x += deltaPosition.x * xSpeed * 0.02f;
        y -= deltaPosition.y * ySpeed * 0.02f;
    }

    public void ZoomCamera(float deltaPinch) {
        if (deltaPinch > lastDist && distance >= minDistance && deltaPinch > 0) {
            distance += deltaPinch * -pinchSpeed / 10;
        }
        else {
            distance += deltaPinch * -pinchSpeed / 10;
        }
        lastDist = deltaPinch;
    }

    void LateUpdate() {
        if (target) {
            distance = Mathf.Clamp(distance, minDistance, maxDistance);

            y = ClampAngle(y, yMinLimit, yMaxLimit);
            Quaternion rotation = Quaternion.Euler(y, x, 0);
            Vector3 vTemp = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * vTemp + target.position;
            //transform.position = Vector3.Lerp(transform.position, position, cameraSpeed * Time.deltaTime);
            transform.position = position;
            transform.rotation = rotation;

        }
    }

    float ClampAngle(float angle, float min, float max) {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }



}