﻿using FlipWebApps.BeautifulTransitions.Scripts.Transitions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUI : MonoBehaviour {

    bool transitioned = false;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
            print("Transition!");
            if (!transitioned) {
                TransitionHelper.TransitionIn(gameObject);
                transitioned = true;
            }
            else {
                TransitionHelper.TransitionOut(gameObject);
                transitioned = false;
            }
        }
	}
}
