﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MenuScript : MonoBehaviour {

    [MenuItem("GameObject/Map Navigation/Destination", false, 10)]
    static void CreateCustomGameObject(MenuCommand menuCommand) {
        // Create a custom game object
        var prefab = Resources.Load("Destination");
        //Instantiate at 0,0,0
        var gPrefab = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
        // Ensure it gets reparented if this was a context click (otherwise does nothing)
        GameObjectUtility.SetParentAndAlign(gPrefab, menuCommand.context as GameObject);
        // Register the creation in the undo system
        Undo.RegisterCreatedObjectUndo(gPrefab, "Create " + gPrefab.name);
        //Assign properties
        Selection.activeObject = gPrefab;
    }
}
