// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "LB_LightingBox/CarBody"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Color("Color", Color) = (0,0,0,0)
		_SpecularIntensity("Specular Intensity", Range( 0 , 1)) = 0
		_Specular("Specular", Color) = (0,0,0,0)
		_Smoothness("Smoothness", Range( 0 , 3)) = 0
		_TexturesCom_PaintedMetal_512_roughness("TexturesCom_PaintedMetal_512_roughness", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color;
		uniform float4 _Specular;
		uniform float _SpecularIntensity;
		uniform sampler2D _TexturesCom_PaintedMetal_512_roughness;
		uniform float4 _TexturesCom_PaintedMetal_512_roughness_ST;
		uniform float _Smoothness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Albedo = _Color.rgb;
			o.Metallic = ( _Specular * _SpecularIntensity ).r;
			float2 uv_TexturesCom_PaintedMetal_512_roughness = i.uv_texcoord * _TexturesCom_PaintedMetal_512_roughness_ST.xy + _TexturesCom_PaintedMetal_512_roughness_ST.zw;
			o.Smoothness = ( tex2D( _TexturesCom_PaintedMetal_512_roughness, uv_TexturesCom_PaintedMetal_512_roughness ) * _Smoothness ).x;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13101
7;29;1010;692;1364.977;519.2911;2.260114;True;False
Node;AmplifyShaderEditor.RangedFloatNode;2;-515,280;Float;False;Property;_SpecularIntensity;Specular Intensity;1;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;5;-514.7252,396.4178;Float;False;Property;_Smoothness;Smoothness;3;0;0;0;3;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;3;-506,95;Float;False;Property;_Specular;Specular;2;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;11;-277.0282,563.5909;Float;True;Property;_TexturesCom_PaintedMetal_512_roughness;TexturesCom_PaintedMetal_512_roughness;4;0;Assets/Garage/Textures/1/TexturesCom_PaintedMetal_512_roughness.tif;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-222,150;Float;False;2;2;0;COLOR;0.0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;180.3386,368.8245;Float;False;2;2;0;FLOAT4;0.0;False;1;FLOAT;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.ColorNode;1;-513.093,-90.88988;Float;False;Property;_Color;Color;0;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;688.1038,-27.421;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;LB_LightingBox/CarBody;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;0;3;0
WireConnection;4;1;2;0
WireConnection;10;0;11;0
WireConnection;10;1;5;0
WireConnection;0;0;1;0
WireConnection;0;3;4;0
WireConnection;0;4;10;0
ASEEND*/
//CHKSM=79A2125BB13A7E5A37F9D84170E22486CD6F1E16