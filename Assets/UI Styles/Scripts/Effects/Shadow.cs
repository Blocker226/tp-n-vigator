﻿using UnityEngine;
using System.Collections;

namespace UIStyles
{
	public class Shadow : UnityEngine.UI.Shadow 
	{
		/*
			This Shadow class just inherits from unitys Shadow, so attaching this class to your Text or Image will be the same as attaching unitys Shadow, 
			Doing it this way you can create your own shadow, use this class for your code or just inherit from your own shadow class and then UI Styles can use your shadow insted of unitys
			
			If you add your own shadow use these variable name so UI Styles can controll the variables, if you need more variables and want help adding them contact me though the settings tab, ill be happy to help.

			public Color	shadowColor		= new Color (0,0,0,.5f);
			public Vector2	shadowDistance	= new Vector2(1, -1);
			public bool		shadowUseAlpha	= true;


			Alternatively you could just use custom components in UI Styles 3.0
		*/
	}
}













